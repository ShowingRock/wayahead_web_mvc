﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace wayahead_web_mvc.Controllers
{
    public class FeedController : Controller
    {
        public IActionResult Feed()
        {
            return View("../Feed/Feed");
        }
    }
}