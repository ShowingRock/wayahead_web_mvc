﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace wayahead_web_mvc.Controllers
{
    public class LoginController : Controller
    {
        public IActionResult Login()
        {
            return PartialView();
        }

        public IActionResult Home()
        {
            return View("../Home/Index");
        }

        public IActionResult Feed()
        {
            return PartialView();
        }
    }
}